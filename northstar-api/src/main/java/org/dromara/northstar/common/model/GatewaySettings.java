package org.dromara.northstar.common.model;

/**
 * 网关配置信息标记
 * @author KevinHuangwl
 *
 * @deprecated 改用org.dromara.northstar.common.GatewaySettings
 */
@Deprecated
public interface GatewaySettings {

}
